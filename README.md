## About

This is a command-line script for downloading and processing movie screenings data from 4 major cinema chains in Poland (Cinema City, Multikino, Helios and Cinema 3D), intended as data source for "Cinema Citizen" web application ([website](http://cctzn.leibrug.pl) | [repository](https://gitlab.com/cctzn/app)).

## Usage

Run `php make_timetables.php --help` to see possible options.

If you are going to run this on your own for testing/experimenting, please consider changing `CURL_USER_AGENT` value first (set in TimetableMaker.php) and limiting queried cinemas/dates (cinema IDs can be found in respective \<Chain\>TimetableMaker.php files). You'd also want to make directories for 'raw' and 'data' files.

Example:
```shell
mkdir tmp
mkdir data
php make_timetables.php --chains cc --cinemas-cc 1088,1086,1092 --days 2 --convert-dir ./data
```

## Output

For each (Chain x Cinema x Date), script saves data file into directory specified in `--convert-dir` option, with filename following the format:
```
<chain>_<cinemaID>_<date:Ymd>.json
```

Inside each file, data is structured as list of movies in the following way:
```json
[
  {
    "title": <String>,
    "duration": <Int>,
    "labels": [<String>],
    "hours": [<Int>]
  },
  ...
]
```
where:

- **duration** is movie length in seconds
- **labels** is chain-specific list of "screening types" like "3D", "dubbing", "Ladies Night" etc. **(currently empty)**
- **hours** is list of [timestamps](http://php.net/manual/en/function.time.php) of screening start times, in ascending order
