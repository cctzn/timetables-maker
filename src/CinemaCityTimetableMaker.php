<?php

class CinemaCityTimetableMaker extends TimetableMaker {
  protected $CINEMAS = [
    1088,
    1086,
    1092,
    1098,
    1089,
    1075,
    1085,
    1065,
    1079,
    1090,
    1076,
    1063,
    1064,
    1094,
    1084,
    1080,
    1081,
    1078,
    1062,
    1082,
    1083,
    1095,
    1077,
    1093,
    1091,
    1074,
    1061,
    1096,
    1070,
    1069,
    1068,
    1060,
    1067,
    1097,
    1087,
  ];
  const RAW_FILE_NAME = 'cc_%s_%s.raw';
  const DOWNLOAD_URL = 'https://www.cinema-city.pl/pl/data-api-service/v1/quickbook/10103/film-events/in-cinema/%s/at-date/%s?lang=pl_PL';
  const DATA_FILE_NAME = 'cc_%s_%s.json';

  protected function download() {
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $rawFileName = sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $curlOpts = [
          CURLOPT_URL => sprintf(self::DOWNLOAD_URL, $cinema, date('Y-m-d', $day)),
        ];
        self::getRawFile($rawFileName, $curlOpts);
      }
    }
  }

  protected function convert() {
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $rawFilePath = self::$OPTIONS['raw_dir'] . '/' . sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $rawData = json_decode(file_get_contents($rawFilePath));
        $data = [];
        foreach ($rawData->body->films as $film) {
          $result = [
            'title' => $film->name,
            'duration' => $film->length * 60,
            'labels' => [], // TODO: labels
            'hours' => array_values(array_map(
              function($event) {
                return strtotime($event->eventDateTime);
              },
              array_filter($rawData->body->events, function($event) use ($film) {
                return $event->filmId === $film->id;
              })
            )),
          ];
          array_push($data, $result);
        }
        $dataFileName = sprintf(self::DATA_FILE_NAME, $cinema, date('Ymd', $day));
        self::saveDataFile($dataFileName, $data);
      }
    }
  }
}
