<?php

class TimetableMaker {
  const CURL_USER_AGENT = 'Cinema Citizen app (cctzn.leibrug.pl)';
  protected static $OPTIONS;
  protected static $DAYS = [];

  public static function initWithOptions($options) {
    self::$OPTIONS = $options;
    self::$OPTIONS['days'] = intval(self::$OPTIONS['days']);
    self::$OPTIONS['days_offset'] = intval(self::$OPTIONS['days_offset']);
    self::setDays();
    if (!isset($options['download_only'])) {
      self::saveTimeBorderFile();
    }
  }

  private static function setDays() {
    $days = self::$OPTIONS['days'];
    $daysOffset = self::$OPTIONS['days_offset'];
    $now = time();
    for ($i = 0; $i < $days; $i++) {
      array_push(self::$DAYS, $now + ($i + $daysOffset) * 86400);
    }
  }

  private static function saveTimeBorderFile() {
    $days = self::$OPTIONS['days'];
    $daysOffset = self::$OPTIONS['days_offset'];
    $timeBorderFilePath = self::$OPTIONS['data_dir'] . '/timeborder.js';
    $timeBorderFileContent = 'CCtzn.setTimeBorders(' .
                             mktime(23, 0, 0, date('n'), (date('j') + $days + $daysOffset - 1)) .
                             '000);' . "\n";
    $timeBorderFile = fopen($timeBorderFilePath, 'w');
    fwrite($timeBorderFile, $timeBorderFileContent);
    fclose($timeBorderFile);
  }

  protected function getRawFile($rawFileName, $curlOpts) {
    $rawFilePath = self::$OPTIONS['raw_dir'] . '/' . $rawFileName;
    $rawFile = fopen($rawFilePath, 'w');
    $connection = curl_init();
    curl_setopt_array($connection, [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_FILE => $rawFile,
      CURLOPT_USERAGENT => self::CURL_USER_AGENT,
    ]);
    curl_setopt_array($connection, $curlOpts);
    curl_exec($connection);
    curl_close($connection);
    fclose($rawFile);
  }

  protected function saveDataFile($dataFileName, $data) {
    $dataFilePath = self::$OPTIONS['data_dir'] . '/' . $dataFileName;
    $dataFile = fopen($dataFilePath, 'w');
    fwrite($dataFile, json_encode($data));
    fclose($dataFile);
  }

  public function setCinemas($cinemas) {
    if (!is_null($cinemas)) {
      $this->CINEMAS = explode(',', $cinemas);
    }
  }

  public function makeTimetables() {
    if (self::$OPTIONS['download_only']) {
      return $this->download();
    }
    if (self::$OPTIONS['convert_only']) {
      return $this->convert();
    }
    $this->download();
    $this->convert();
  }
}
