<?php

class Cinema3dTimetableMaker extends TimetableMaker {
  protected $CINEMAS = [
    'biala-podlaska',
    'gdansk',
    'glogow',
    'gorzow-wlkp',
    'kalisz',
    'klodzko',
    'leszno',
    'mielec',
    'swidnica',
    'swinoujscie',
    'tarnow',
    'warszawa-atrium-reduta',
  ];
  const RAW_FILE_NAME = 'c3d_%s_%s.raw';
  const DOWNLOAD_URL = 'http://system.v67926.tld.pl/api/v1/seances/%s/%s';
  const DATA_FILE_NAME = 'c3d_%s_%s.json';

  protected function download() {
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $rawFileName = sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $curlOpts = [
          CURLOPT_URL => sprintf(self::DOWNLOAD_URL, $cinema, date('Y-m-d', $day)),
        ];
        self::getRawFile($rawFileName, $curlOpts);
      }
    }
  }

  protected function convert() {
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $rawFilePath = self::$OPTIONS['raw_dir'] . '/' . sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $rawData = json_decode(file_get_contents($rawFilePath));
        $data = [];
        foreach ($rawData as $film) {
          // TODO: if not $film->movie, this looks like a single special screening
          //       - find out how to handle (it can have 'null' duration)
          if (isset($film->movie)) {
            $result = [
              'title' => $film->movie->title,
              'duration' => $film->movie->duration * 60,
              'labels' => [], // TODO: labels
              'hours' => array_values(array_map(
                function($seance) {
                  return strtotime($seance->start);
                },
                $film->seances
              )),
            ];
            array_push($data, $result);
          }
        }
        $dataFileName = sprintf(self::DATA_FILE_NAME, $cinema, date('Ymd', $day));
        self::saveDataFile($dataFileName, $data);
      }
    }
  }
}
