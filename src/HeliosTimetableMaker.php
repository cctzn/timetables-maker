<?php

class HeliosTimetableMaker extends TimetableMaker {
  protected $CINEMAS = [
    37,
    26,
    6,
    46,
    7,
    38,
    27,
    2,
    58,
    49,
    42,
    9,
    10,
    32,
    45,
    40,
    51,
    34,
    13,
    14,
    55,
    61,
    30,
    31,
    47,
    39,
    16,
    17,
    60,
    28,
    19,
    29,
    50,
    52,
    20,
    36,
    21,
    43,
    22,
    56,
    44,
    23,
    35,
    33,
    53,
    57,
    54,
    48,
    3,
    64,
  ];
  const RAW_FILE_NAME = 'h_%s_%s.raw';
  const DOWNLOAD_URL = 'https://www.helios.pl/%s/Repertuar/axRepertoire/?dzien=%s';
  const DATA_FILE_NAME = 'h_%s_%s.json';

  protected function download() {
    // TODO: no known way of getting data with negative days_offset
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $downloadUrlDay = array_search($day, self::$DAYS) + self::$OPTIONS['days_offset'];
        $rawFileName = sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $curlOpts = [
          CURLOPT_URL => sprintf(self::DOWNLOAD_URL, $cinema, $downloadUrlDay),
        ];
        self::getRawFile($rawFileName, $curlOpts);
      }
    }
  }

  protected function convert() {
    libxml_use_internal_errors(true);
    foreach ($this->CINEMAS as $cinema) {
      foreach (self::$DAYS as $day) {
        $rawFilePath = self::$OPTIONS['raw_dir'] . '/' . sprintf(self::RAW_FILE_NAME, $cinema, date('Ymd', $day));
        $rawData = new DOMDocument;
        $rawData->loadHTML(
          mb_convert_encoding(
            json_decode(
              file_get_contents($rawFilePath)
            )->html,
            'HTML-ENTITIES',
            'UTF-8'
          )
        );
        $data = [];
        foreach($rawData->getElementsByTagName('ul')->item(2)->childNodes as $film) {
          if (strpos($film->textContent, 'Czas trwania') !== false) {
            preg_match(
              '/Czas trwania: (\d+)/',
              (function($divs) {
                foreach ($divs as $div) {
                  if ($div->attributes->getNamedItem('class')->value === 'movie-description') {
                    return $div->textContent;
                  }
                }
              })($film->getElementsByTagName('div')),
              $durationMatches
            );
            preg_match_all(
              '/\d{2}:\d{2}/',
              $film->lastChild->previousSibling->textContent,
              $hoursMatches
            );
            $result = [
              'title' => trim($film->getElementsByTagName('h2')->item(0)->textContent), // TODO: filter out labels
              'duration' => intval($durationMatches[1]) * 60,
              'labels' => [], // TODO: labels
              'hours' => array_map(function($hour) use ($day) {
                return strtotime(date('d-m-Y', $day) . ' ' . $hour);
              }, $hoursMatches[0]),
            ];
            array_push($data, $result);
          }
        }
        $dataFileName = sprintf(self::DATA_FILE_NAME, $cinema, date('Ymd', $day));
        self::saveDataFile($dataFileName, $data);
      }
    }
  }
}
