<?php

class MultikinoTimetableMaker extends TimetableMaker {
  protected $CINEMAS = [
    3,
    37,
    2,
    4,
    5,
    39,
    33,
    6,
    7,
    38,
    21,
    40,
    8,
    9,
    10,
    26,
    25,
    11,
    30,
    32,
    12,
    13,
    35,
    43,
    14,
    19,
    15,
    1,
    16,
    17,
    18,
    20,
    34,
  ];
  const RAW_FILE_NAME = 'mk_%s.raw';
  const DOWNLOAD_URL = 'https://multikino.pl/data/filmswithshowings/%s';
  const DATA_FILE_NAME = 'mk_%s_%s.json';

  protected function download() {
    foreach ($this->CINEMAS as $cinema) {
      $rawFileName = sprintf(self::RAW_FILE_NAME, $cinema);
      $curlOpts = [
        CURLOPT_URL => sprintf(self::DOWNLOAD_URL, $cinema),
      ];
      self::getRawFile($rawFileName, $curlOpts);
    }
  }

  protected function convert() {
    foreach ($this->CINEMAS as $cinema) {
      $rawFilePath = self::$OPTIONS['raw_dir'] . '/' . sprintf(self::RAW_FILE_NAME, $cinema);
      $rawData = json_decode(file_get_contents($rawFilePath));
      $dates = [];
      foreach ($rawData->films as $film) {
        if (
          $film->showing_type->name === 'Filmy'
          && count($film->showings)
          && $film->info_runningtime
        ) {
          foreach ($film->showings as $showing) {
            if (!isset($dates[$showing->date_time])) {
              $dates[$showing->date_time] = [];
            }
            $dates[$showing->date_time][$film->id] = [
              'title' => $film->title,
              'duration' => intval($film->info_runningtime) * 60,
              'labels' => [], // TODO: labels
              'hours' => array_map(function($time) {
                return strtotime($time->date);
              }, $showing->times),
            ];
          }
        }
      }
      foreach ($dates as $date => $results) {
        // TODO: limit dates?
        $dataFileName = sprintf(self::DATA_FILE_NAME, $cinema, str_replace('-', '', $date));
        $data = array_values($results);
        self::saveDataFile($dataFileName, $data);
      }
    }
  }
}
