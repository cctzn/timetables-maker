<?php

require __DIR__ . '/vendor/autoload.php';

spl_autoload_register(function($className) {
  require './src/' . $className . '.php';
});

$getopt = new \GetOpt\GetOpt([

  \GetOpt\Option::create(null, 'help', \GetOpt\GetOpt::NO_ARGUMENT)
    ->setDescription('Display help'),

  \GetOpt\Option::create(null, 'chains', \GetOpt\GetOpt::REQUIRED_ARGUMENT)
    ->setDescription(
      'Cinema chains to process as comma-separated chain IDs [cc|mk|h|c3d]; ' .
      'by default process all chains'
    )
    ->setArgumentName('chains')
    ->setDefaultValue('cc,mk,h,c3d')
    ->setValidation(function($value) {
      foreach(explode(',', $value) as $chain) {
        if (!in_array($chain, ['cc', 'mk', 'h', 'c3d'])) {
          return false;
        }
      }
      return true;
    }),

  \GetOpt\Option::create(null, 'download-only', \GetOpt\GetOpt::NO_ARGUMENT)
    ->setDescription('Only download raw files'),

  \GetOpt\Option::create(null, 'convert-only', \GetOpt\GetOpt::NO_ARGUMENT)
    ->setDescription('Only convert raw files to data files'),

  \GetOpt\Option::create(null, 'days-offset', \GetOpt\GetOpt::REQUIRED_ARGUMENT)
    ->setArgumentName('num')
    ->setDescription(
      'Offset for starting day of processing; ' .
      'by default 0 (start at today)'
    )
    ->setDefaultValue(0)
    ->setValidation('is_numeric'),

  \GetOpt\Option::create(null, 'days', \GetOpt\GetOpt::REQUIRED_ARGUMENT)
    ->setDescription(
      'Number of days to process; ' .
      'by default 7 (process a whole week)'
    )
    ->setArgumentName('num')
    ->setDefaultValue(7)
    ->setValidation('is_numeric'),

  \GetOpt\Option::create(null, 'download-dir', \GetOpt\GetOpt::REQUIRED_ARGUMENT)
    ->setDescription('Directory path for raw files')
    ->setArgumentName('path')
    ->setDefaultValue('./tmp'),

  \GetOpt\Option::create(null, 'convert-dir', \GetOpt\GetOpt::REQUIRED_ARGUMENT)
    ->setDescription('Directory path for converted data files')
    ->setArgumentName('path')
    ->setDefaultValue('../www/data'),

  \GetOpt\Option::create(null, 'cinemas-cc', \GetOpt\GetOpt::OPTIONAL_ARGUMENT)
    ->setDescription(
      'Cinema City cinema IDs to process as comma-separated cinema IDs; ' .
      'by default process all cinemas'
    )
    ->setArgumentName('cinemas'),

  \GetOpt\Option::create(null, 'cinemas-mk', \GetOpt\GetOpt::OPTIONAL_ARGUMENT)
    ->setDescription(
      'Multikino cinema IDs to process as comma-separated cinema IDs; ' .
      'by default process all cinemas'
    )
    ->setArgumentName('cinemas'),

  \GetOpt\Option::create(null, 'cinemas-h', \GetOpt\GetOpt::OPTIONAL_ARGUMENT)
    ->setDescription(
      'Helios cinema IDs to process as comma-separated cinema IDs; ' .
      'by default process all cinemas'
    )
    ->setArgumentName('cinemas'),

  \GetOpt\Option::create(null, 'cinemas-c3d', \GetOpt\GetOpt::OPTIONAL_ARGUMENT)
    ->setDescription(
      'Cinema 3D cinema IDs to process as comma-separated cinema IDs; ' .
      'by default process all cinemas'
    )
    ->setArgumentName('cinemas'),

]);

$getopt->process();

if ($getopt->getOption('help')) {
  echo $getopt->getHelpText();
  exit;
}

TimetableMaker::initWithOptions([
  'download_only' => $getopt->getOption('download-only'),
  'convert_only' => $getopt->getOption('convert-only'),
  'days_offset' => $getopt->getOption('days-offset'),
  'days' => $getopt->getOption('days'),
  'raw_dir' => $getopt->getOption('download-dir'),
  'data_dir' => $getopt->getOption('convert-dir'),
]);

$chains = explode(',', $getopt->getOption('chains'));

if (in_array('cc', $chains)) {
  $cinemaCityTimetableMaker = new CinemaCityTimetableMaker;
  $cinemaCityTimetableMaker->setCinemas($getopt->getOption('cinemas-cc'));
  $cinemaCityTimetableMaker->makeTimetables();
}

if (in_array('mk', $chains)) {
  $multikinoTimetableMaker = new MultikinoTimetableMaker;
  $multikinoTimetableMaker->setCinemas($getopt->getOption('cinemas-mk'));
  $multikinoTimetableMaker->makeTimetables();
}

if (in_array('h', $chains)) {
  $heliosTimetableMaker = new HeliosTimetableMaker;
  $heliosTimetableMaker->setCinemas($getopt->getOption('cinemas-h'));
  $heliosTimetableMaker->makeTimetables();
}

if (in_array('c3d', $chains)) {
  $cinema3dTimetableMaker = new Cinema3dTimetableMaker;
  $cinema3dTimetableMaker->setCinemas($getopt->getOption('cinemas-c3d'));
  $cinema3dTimetableMaker->makeTimetables();
}
